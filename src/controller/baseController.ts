import sql, { ConnectionPool } from 'mssql';

const config = {
  user: "sa",
  password: "viet221100",
  server: 'localhost',
  database: "API_BTL_QLyBanSach",
  options: {
    enableArithAbort: true,
    trustServerCertificate: true,
    encrypt: true
  }
};

export const pool: ConnectionPool = new sql.ConnectionPool(config);

pool.connect().then(() => {
  console.log('Connected to SQL Server');
}).catch(err => {
  console.error('Error connecting to SQL Server:', err);
});