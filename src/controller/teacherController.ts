// import { pool } from './baseController';
// import { Request , Response} from 'express';


// export const createTeacher = async (request: Request, response: Response) => {
//     try {
//         const { teachercode, name, exp, subjectcode} = request.body;
//         if(teachercode == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap teachercode "
//             });
//         }
//         let rs = await pool.query(`INSERT INTO public.teacher(teachercode, "name", "exp", subjectcode) 
//         VALUES($1, $2, $3,$4)`,[teachercode, name, exp, subjectcode]);       // sends queries
//         return response.json({
//             status: true,
//             messeger: "them data thanh cong"
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     } 
// };
// export const getTeacher = async (request: Request, response: Response) => {
//     try {
//         let rs = await pool.query(`select *
//         from teacher`)      // sends queries
//         return response.json({
//             status: true,
//             messeger: "truy xuat thanh cong",
//             data: rs.rows
//         });
//     }catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     }  
// };
// export const getTeacherTestcode = async (request: Request, response: Response) => {
//     try {
//         const { testcode } = request.body;
//         let query = `select teacher.teachercode,teacher.name, "exp", subjectcode
//         from teacher left join test on teacher.teachercode = test.teachercode 
//         where testcode `
//         let param = [];
//         if (testcode == null){
//             query += `is null`;
//         } else {
//             query += `= $1`;
//             param.push(testcode)
//         }
//         let rs = await pool.query(query, param);       // sends queries
//         return response.json({
//             status: true,
//             messeger: "truy xuat thanh cong",
//             data: rs.rows
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     } 
// };
// export const updateTeacher = async (request: Request, response: Response) => {
//     try {
//         const { name, exp, subjectcode,teachercode } = request.body;
//         if(teachercode == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap teachercode "
//             });
//         }
        
//         let rs = await pool.query(`UPDATE teacher SET  name = $1, exp = $2,subjectcode =$3 WHERE teachercode = $4`,
//         [name, exp, subjectcode,teachercode],);       // sends queries
//         return response.json({
//             status: true,
//             messeger: "update thanh cong",
//             data: rs.rows
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     } 
// };
// export const deleteTeacher = async (request: Request, response: Response) => {
//     try {
//         const { teachercode } = request.body;
//         if(teachercode == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap teachercode"
//             });
//         }
//         let rs = await pool.query(`DELETE FROM public.teacher
//         WHERE teachercode=$1`,[teachercode]);       // sends queries
//         if(rs.rowCount == 0)   {
//             return response.json({
//                 status: true,
//                 messeger: "khong co gia tri",
//                 data: rs.rows
//         }); 
//     }
//         return response.json({
//             status: true,
//             messeger: "delete thanh cong",
//             data: rs.rows
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     } 
// };
