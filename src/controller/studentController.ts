import moment from 'moment';
import { pool } from './baseController';
import { Request, Response } from 'express';

// export const createStudent = async (request: Request, response: Response) => {
//     try {
//         const { studentcode, name, birth } = request.body;
//         if (studentcode == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap studentcode "
//             });
//         }
//         if (birth == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap date "
//             });
//         } else {
//             // neu birth k phai dang date thi bao loi
//             var validateBirth = moment(birth, "YYYY-MM-DD").isValid();
//             if (validateBirth == false) {
//                 return response.json({
//                     status: false,
//                     messeger: "birth khong dung dinh dang date YYYY-MM-DD"
//                 });
//             }
//         }

//         let rs = await pool.query(`INSERT INTO public.student ("studentcode", "name", "birth")
//         VALUES($1, $2, $3)`, [studentcode, name, birth]);       // sends queries
//         return response.json({
//             status: true,
//             messeger: "them data thanh cong"
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     }
// };
export const getStudent = async (request: Request, response: Response) => {
    try {
        const req = pool.request();
        let rs = await req.query(`select *
        from tSach ` )
        // sends queries
        return response.json({
            status: true,
            messeger: "truy xuat thanh cong",
            data: rs.recordset
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return response.json({
            status: false,
            message: message,
        });
    }
};
// export const getStudentTestcode = async (request: Request, response: Response) => {
//     try {
//         const { testcode } = request.body;
//         if (testcode == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap testcode "
//             });
//         }
//         let rs = await pool.query(`select *
//         from student inner join "studentTest" on student.studentcode = "studentTest".studentcode
//         where testcode = $1`, [testcode]);       // sends queries
//         return response.json({
//             status: true,
//             messeger: "truy xuat thanh cong",
//             data: rs.rows
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     }
// };
// export const updateStudent = async (request: Request, response: Response) => {
//     try {
//         const { name, birth, studentcode } = request.body;
//         if (studentcode == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap studentcode "
//             });
//         }
//         let rs = await pool.query(`UPDATE student SET  name = $1, birth = $2 WHERE studentcode = $3`,
//             [name, birth, studentcode],);       // sends queries
//         return response.json({
//             status: true,
//             messeger: "update thanh cong",
//             data: rs.rows
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     }
// };
// export const deleteStudent = async (request: Request, response: Response) => {
//     try {
//         const { studentcode } = request.body;
//         if (studentcode == undefined) {
//             return response.json({
//                 status: false,
//                 messeger: "chua nhap studentcode"
//             });
//         }
//         let rs = await pool.query(`DELETE FROM public.student
//         WHERE studentcode=$1`, [studentcode]);       // sends queries
//         if (rs.rowCount == 0) {
//             return response.json({
//                 status: true,
//                 messeger: "khong co gia tri",
//                 data: rs.rows
//             });
//         }
//         return response.json({
//             status: true,
//             messeger: "delete thanh cong",
//             data: rs.rows
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return response.json({
//             status: false,
//             message: message,
//         });
//     }
// };


