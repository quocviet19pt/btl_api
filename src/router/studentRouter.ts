import express from 'express';
import { getStudent } from '../controller/studentController';

const router = express.Router();


// router.post('/create',  createStudent);
router.post('/get', getStudent);
// router.post('/getTestcode',  getStudentTestcode);
// router.post('/update',  updateStudent);
// router.post('/delete',  deleteStudent);
export default router;