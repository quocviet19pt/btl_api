import express from "express";

import studentRouter from "./router/studentRouter";
// import teacherRouter from "./router/teacherRouter";
// import testRouter from "./router/testRouter";
// import subjectRouter from "./router/subjectRouter";
// import studentSubjectRouter from "./router/studentSubjectRouter";
// import studentTestRouter from "./router/studentTestRouter";
// import userRouter from "./router/userRouter";


const app = express();
app.use(express.json());

app.use("/api/student", studentRouter)
// app.use("/api/v1/teacher", teacherRouter)
// app.use("/api/v1/test", testRouter)
// app.use("/api/v1/subject", subjectRouter)
// app.use("/api/v1/studentSubject", studentSubjectRouter)
// app.use("/api/v1/studentTest", studentTestRouter)
// app.use("/api/v1/user", userRouter)

const server = app.listen(7000, () => {
    console.log(`Localhost đã chạy ở port ${7000}`);
});
